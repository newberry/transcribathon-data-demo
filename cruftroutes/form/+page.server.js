import {
    prevent_default
} from 'svelte/internal';
import prisma from '$utils/prisma'
import {
    json
} from '@sveltejs/kit'
// export async function load({ cookies }) {
//     const user = await db.getUserFromSession(cookies.get('sessionid'));
//     return { user };
//   }
export const actions = {
    search: async ({
        request
    }) => {
        prevent_default()

        const data = await request.formData();
        const name = data.get('name');
        // const password = data.get('password');

        const results = await prisma.yearlines.findMany({
            where: {
                text: {
                    contains: name
                }
            }
        });

        // TODO log the user in
        return json(results)
    }
};