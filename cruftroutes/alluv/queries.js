import prisma from '$utils/prisma'

export async function findWord(kw) {

    const results = await prisma.yearlines.findMany({
        where: {
            text: {
                contains: kw
            }
        }
    })
    return results
  }
  