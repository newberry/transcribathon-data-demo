import prisma from '$utils/prisma'

export async function load({params}){

    const {rest} = params
    
    let paramArray = rest.indexOf('/') ? rest.split('/') : [rest]

    let filters = {years: [], terms: []}

    let sorter = paramArray.map(f=> f.match(/^([0-9]{4})$/)? filters.years.push(f) : filters.terms.push(f)) 

    let query = Object.keys(filters).map(f => filters[f].map(ff => f === 'years' ? ({year:  parseInt(ff)}) : ({text: {contains: ff}}) )).flat()
    console.log(filters)

    const results = await prisma.yearwords.findMany({
        where: { OR:  query}           
    })

    return {results, filters}
}

