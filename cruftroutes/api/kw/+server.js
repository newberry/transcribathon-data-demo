import prisma from '$utils/prisma'
import { json } from '@sveltejs/kit'

export async function GET({url}) {

	let { keyword } = url.searchParams
  	const results = await prisma.yearlines.findMany({
		where: {
            text: {
                contains: keyword
            }
        }
	});

  return json(results)
}
