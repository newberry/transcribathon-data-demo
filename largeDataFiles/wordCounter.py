#!/bin/python
import json, re
import numpy as np 
import pandas as pd

  
f = open('word_arrays.json')
  
data = json.load(f)
short_stop_list = ["a","about","actually","almost","also","although","always","am","an","and","any","are","as","at","be","became","become","but","by","can","could","did","do","does","each","either","else","for","from","had","has","have","hence","how","i","if","in","is","it","its","just","may","maybe","me","might","mine","must","my","mine","must","my","neither","nor","not","of","oh","ok","when","where","whereas","wherever","whenever","whether","which","while","who","whom","whoever","whose","why","will","with","within","without","would","yes","yet","you","your"]
stop_list = ['a','able','about','across','after','all','almost','also','am','among','an','and','any','are','as','at','be','because','been','but','by','can','cannot','could','dear','did','do','does','either','else','ever','every','for','from','get','got','had','has','have','he','her','hers','him','his','how','however','i','if','in','into','is','it','its','just','least','let','like','likely','may','me','might','most','must','my','neither','no','nor','not','of','off','often','on','only','or','other','our','own','rather','said','say','says','she','should','since','so','some','than','that','the','their','them','then','there','these','they','this','tis','to','too','twas','us','wants','was','we','were','what','when','where','which','while','who','whom','why','will','with','would','yet','you','your']
reduced_list = [[], []]
for w in data: 
    w = re.sub(r'\W+', '', w).lower()
    if len(w) > 1:
        if w in short_stop_list:
            reduced_list[0].append(w)
        else: 
            reduced_list[1].append(w)


counts = pd.value_counts(np.array(reduced_list[0]))
counts.to_json(r'word_counts_stopwords.json')
counts = pd.value_counts(np.array(reduced_list[1]))
counts.to_json(r'word_counts.json')

# with open("word_counts.json", "w") as outfile:
#     json.dump(jsonmode, outfile, indent=4)