#!/Library/Frameworks/Python.framework/Versions/3.9/bin/python3
#/bin/python
import json, re
import pandas as pd  


f = open('just_transc.json')
data = json.load(f)
just_words = []
for line in data:
    for w in line.split(' '):
        if '\n' in w:
            for n in w.split('\n'):
                n = re.sub(r'\W+', '', n).lower()
                if len(n) > 0 :
                    just_words.append(n)
        else: 
            w = re.sub(r'\W+', '', w).lower()
            if len(w) > 0:
                just_words.append(w)


dict = {'words': just_words}  
df = pd.DataFrame(dict)
df.to_csv('sqlimport_just_words.csv') 
