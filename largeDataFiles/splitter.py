import numpy as np 
import pandas as pd

import json
  
f = open('just_transc.json')
  
data = json.load(f)
  
just_words = []

for i in data:
    for w in i.split(' '):
        if '\n' in w:
            for n in w.split('\n'):
                just_words.append(n)
        else: 
            just_words.append(w)


with open("word_arrays.json", "w") as outfile:
    json.dump(just_words, outfile, indent=4)