First I took the two large datasets - one from before we wiped the server for a great reset, and the other, which is all the work since then - and combined them;

Then, I did a simple word count.  I used python for most of the data manipulation - it's probably the best tool for that job, and, while my python writing skills is like dragging a sink through the woods, it still only took a couple of minutes to:
1. extract the actual transcriptions from the json file
2. slice each line on ' ' and '\n'
3. count the words
    a. you can do this with a pen and paper, obviously, but I chose to use the computer - 
    b. pandas is a python module that's great for manipulating data like this; with that and numpy (which is, as the name implies, a set of mathmatical tools in python),

These are the scripts I wrote for this.  Again, my python is inelegant - I'm sure there are much better ways to do this, but writing all these scripts took like 5 minutes, and running them took less than that, so I'm not too worried about it.

fileShortener.py : takes the transcription field out of the larger json file
splitter.py : slices each transcription by ' ' and '\n' to use for high level word frequency count
dateFinder.py : uses a simple regular expression to find 4-digit numbers; if the number is less than 2022, we'll call that a year 
    (most of these documents are from the last couple hundred years, so you could easily filter more accurately; I chose darkness)

dateShuffler.py : takes each unique year and dumps it into a list; then takes each year and makes it a key, and assigns each transcription text to that year, (and slices that text to )