#!/Library/Frameworks/Python.framework/Versions/3.9/bin/python3
#/bin/python
import json, re, math

f = open('years_and_words_gt4.json')
data = json.load(f)

master_word_list = {}
output_data = {}

for year in data:
    counter = 0
    for word in data[year]:
        if counter < 10:
            if year in output_data:
                output_data[year][word] = data[year][word]
            else: 
                output_data[year] = {}
                output_data[year][word] = data[year][word]
            if word in master_word_list:
                master_word_list[word] = master_word_list[word] + data[year][word]
            else: 
                master_word_list[word] = data[year][word]
            counter = counter + 1
            print(counter)
# output_dict = {}
master_word_list = dict(sorted(master_word_list.items(), key=lambda item: item[1], reverse=True))

with open("../src/data/trunc_master_word_list.json", "w") as outfile:
    json.dump(master_word_list, outfile, indent=4)
with open("../src/data/truncated_list.json", "w") as outfile:
    json.dump(output_data, outfile, indent=4)