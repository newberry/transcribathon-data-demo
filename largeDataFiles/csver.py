#!/Library/Frameworks/Python.framework/Versions/3.9/bin/python3
#/bin/python
import json, re
import pandas as pd  


f = open('transc_with_years_norounding.json')
data = json.load(f)

output_list = []
years = []
words = []
# lines= []
# linelength = 0
# longestlineidx = 0
for idx, i in enumerate(data):
    # if linelength < len(i[1]):
    #     linelength = len(i[1])
    #     longestlineidx = idx
    #     print('new lngest line: ' + str(linelength))
    #     print('index of that line is ' + str(idx))
    just_words = []
    for w in i[1].split(' '):
        if '\n' in w:
            for n in w.split('\n'):
                n = re.sub(r'\W+', '', n).lower()
                if len(n) > 0 :
                    just_words.append(n)
        else: 
            w = re.sub(r'\W+', '', w).lower()
            if len(w) > 0:
                just_words.append(w)
    for y in i[0]:
        # line = i[1].replace('\n',' ')
        # years.append(y)
        # lines.append(line)
        for w in just_words: 
            years.append(y)
            words.append(w)
            # retval = [y, w]
            # output_list.append(retval)
# print(data[longestlineidx])

# dict = {'year': years, 'lines': lines}  
# df = pd.DataFrame(dict)
# df.to_csv('sqlimport_lines.csv') 

dict = {'year': years, 'words': words}  
df = pd.DataFrame(dict)
df.to_csv('sqlimport_words.csv') 
