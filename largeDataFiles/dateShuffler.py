#!/Library/Frameworks/Python.framework/Versions/3.9/bin/python3
#/bin/python
import json, re, math

f = open('transc_with_years.json')
data = json.load(f)

years = []

for i in data:
    for y in i[0]:
        # y = math.floor(int(y) / 10) * 10
        if y not in years:
            years.append(y)

years.sort()
year_dict = {y: {} for y in years}


# g = open('word_counts_short.json')
g = open('word_counts_short.json')
all_words_counted = json.load(g)
all_words_list = list(all_words_counted.keys())

for i in data: 
    just_words = []
    for w in i[1].split(' '):
        if '\n' in w:
            for n in w.split('\n'):
                n = re.sub(r'\W+', '', n).lower()
                if len(n) > 0 and n in all_words_list:
                    just_words.append(n)
        else: 
            w = re.sub(r'\W+', '', w).lower()
            if len(w) > 0 and w in all_words_list:
                just_words.append(w)
    for y in i[0]: 
        year = math.floor(int(y) / 10) * 10
        word_arrays = {}
        for jw in just_words:
            if jw not in year_dict[year]:
                year_dict[year][jw] = 1
            else: 
                year_dict[year][jw] = year_dict[year][jw] + 1

        #     year_dict[year].append(wa)
        # for w in just_words:
        #     if w in word_arrays: 
        #         word_arrays[w] = word_arrays[w] + 1
        #     else: 
        #         word_arrays[w] = 1
        # print(year_dict[year])

# print(year_dict)

counts_gt_4 =  {y: {} for y in years}
for cgi in counts_gt_4:
    for w in year_dict[cgi]:
        if year_dict[cgi][w] > 10 and len(w) > 1: 
            counts_gt_4[cgi][w] = year_dict[cgi][w]

for cgt in counts_gt_4:
    counts_sorted = dict(sorted(counts_gt_4[cgt].items(), key=lambda item: item[1], reverse=True))
    counts_gt_4[cgt] = counts_sorted

with open("years_and_words_gt4.json", "w") as outfile:
    json.dump(counts_gt_4, outfile, indent=4)