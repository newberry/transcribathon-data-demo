import json, re 


f = open('transc_with_years_1500.json')
data = json.load(f)


years = []

for i in data:
    for y in i[0]:
        if y not in years:
            years.append(y)

years.sort()
year_dict = {y: 0 for y in years}


for i in data:
    for year in i[0]:
        just_words = []
        for w in i[1].split(' '):
            if '\n' in w:
                for n in w.split('\n'):
                    n = re.sub(r'\W+', '', n).lower()
                    just_words.append(n)
            else: 
                w = re.sub(r'\W+', '', w).lower()
                just_words.append(w)
        word_count = len(just_words)
        year_dict[year] = year_dict[year] + word_count

with open("words_per_year.json", "w") as outfile:
    json.dump(year_dict, outfile, indent=4)