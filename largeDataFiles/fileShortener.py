import numpy as np 
import pandas as pandas

import json
  
f = open('itemTranscriptions_all_upto_230412.json')
data = json.load(f)

just_transc = []

for i in data['transcriptions']:
    for t in i['pages']:
        just_transc.append(t['transcription'])
f.close()

with open("just_transc.json", "w") as outfile:
    json.dump(just_transc, outfile, indent=4)