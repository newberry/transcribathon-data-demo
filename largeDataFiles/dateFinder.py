#!/Library/Frameworks/Python.framework/Versions/3.9/bin/python3
#/bin/python
import numpy as np 
import pandas as pandas

import json, re, math
  
f = open('itemTranscriptions_all_upto_230412.json')
data = json.load(f)

transc_with_years = []

for i in data['transcriptions']:
    for t in i['pages']:
        srchr = re.compile('([0-9]{4})')
        possible_year = srchr.findall(t['transcription'])
        if possible_year:
            only_real_years = []
            for y in possible_year:
                if int(y) > 1700 and int(y) < 2000:
                    # y = math.floor(int(y) / 10) * 10
                    if  y not in only_real_years:
                        only_real_years.append(y)
            if len(only_real_years) > 0:
                outputarr = [only_real_years, t['transcription']]
                transc_with_years.append(outputarr)

print(len(transc_with_years))

f.close()

with open("transc_with_years_norounding.json", "w") as outfile:
    json.dump(transc_with_years, outfile, indent=4)