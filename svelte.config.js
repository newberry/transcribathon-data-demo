import adapter from '@sveltejs/adapter-auto';
import preprocess from 'svelte-preprocess';
import path from "path"

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter(),
		alias: {
		  $lib: path.resolve('./src/lib'),
		  $utils: path.resolve('./src/lib/utils'),
		  $data: path.resolve('./src/data'),
		  $src: path.resolve('./src')
		}
	}
};

export default config;