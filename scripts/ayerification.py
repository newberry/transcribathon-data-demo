#!/Library/Frameworks/Python.framework/Versions/3.9/bin/python3
#/bin/python

import json

f = open('itemTranscriptions_221231.json')
data = json.load(f)

ayers = []

for item in data["transcriptions"]:
    ayerornot = False
    for page in item["pages"]:
        if 'ayer' in page["pageorigfilename"]:
            ayerornot = True
    if ayerornot:
        ayers.append(item)

with open("ayerdata.json", "w") as outfile:
    json.dump(ayers, outfile, indent=4)