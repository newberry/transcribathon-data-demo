#!/Library/Frameworks/Python.framework/Versions/3.9/bin/python3
#/bin/python

import json, re
import pandas as pd  

f = open('ayerdata.json')
data = json.load(f)

ayer_lines = []
ayer_words = []
ayercsv = []

for item in data:
    for page in item["pages"]:
        years = []
        if "year" in page: 
            for year in page["year"]:
                if year not in years:
                    years.append(year) 
            for year in years: 
                outputArray_lines = [item["id"], page["pageid"], page["transcription"].replace('\n', ' '), year ]
                ayer_lines.append(outputArray_lines)
        
            just_words = []
            for w in page["transcription"].split(' '):
                if '\n' in w:
                    for n in w.split('\n'):
                        n = re.sub(r'\W+', '', n).lower()
                        if len(n) > 0:
                            just_words.append(n)
                else: 
                    w = re.sub(r'\W+', '', w).lower()
                    if len(w) > 0:
                        just_words.append(w)
            for year in years: 
                for jw in just_words:
                    outputArray_words = [ item["id"], page["pageid"], jw, year ]
                    ayer_words.append(outputArray_words)
        else: 
            year = 0
            outputArray_lines = [item["id"], page["pageid"], page["transcription"].replace('\n', ' '), year ]
            ayer_lines.append(outputArray_lines)
        
            just_words = []
            for w in page["transcription"].split(' '):
                if '\n' in w:
                    for n in w.split('\n'):
                        n = re.sub(r'\W+', '', n).lower()
                        if len(n) > 0:
                            just_words.append(n)
                else: 
                    w = re.sub(r'\W+', '', w).lower()
                    if len(w) > 0:
                        just_words.append(w)
            for jw in just_words:
                year = 0
                outputArray_words = [ item["id"], page["pageid"], jw, year ]
                ayer_words.append(outputArray_words)

# with open("ayerdata.json", "w") as outfile:
#     json.dump(ayers, outfile, indent=4)

t_id = []
t_pageid = []
transc = []
year = []

# longest = 0
for al in ayer_lines: 
    # if len(al[2]) > longest:
    #     longest = len(al[2])
    #     print(longest)
    t_id.append(al[0])
    t_pageid.append(al[1])
    transc.append(al[2])
    year.append(al[3])

dict = {'id': t_id, 'pageid': t_pageid, 'transc': transc, 'year': year}  
df = pd.DataFrame(dict)
df.to_csv('ayer_sqlimport_lines.csv')

t_id = []
t_pageid = []
word = []
year = []

for al in ayer_words:  
    t_id.append(al[0])
    t_pageid.append(al[1])
    word.append(al[2])
    year.append(al[3])

dict = {'id': t_id, 'pageid': t_pageid, 'word': word, 'year': year}  
df = pd.DataFrame(dict)
df.to_csv('ayer_sqlimport_words.csv')
