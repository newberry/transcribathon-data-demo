import prisma from '$utils/prisma'

export async function load({
  params
}) {
  const results = await prisma.ayerlines.findMany({
    where: {
      text: {
        contains: params.keyword
      }
    }
  })
  return {
    results
  }
}