import prisma from '$utils/prisma'

export async function load({params}){
    const {kw} = params
    const results = await prisma.ayerwords.findMany()
    return {results}
  }


//   export async function load({params}){
//     const {kw} = params
//     const results = await prisma.ayerlines.findMany({
//         where: {
//             text: {
//                 contains: kw
//             }
//         }
//     })
//     return {results}
//   }

