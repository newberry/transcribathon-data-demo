import prisma from '$utils/prisma'

export async function load({params}){
    console.log(params)
    const {word} = params
    let words = word.indexOf('/') ? word.split('/') : [word]
    let query = words.map(w => ({text: {contains: w}}))
    const results = await prisma.yearwords.findMany({
        where: { OR: query}
           
    })
    return {results, words}
  }

