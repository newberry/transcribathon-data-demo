import prisma from '$utils/prisma'

export async function load({params}){
  const keywords = params.keywords
  console.log(keywords) 
    let kw = keywords.indexOf('/') ? keywords.split('/') : [keywords]
    let query = kw.map(k => ({text: {contains: k}}))
  console.log() 
  const results = await prisma.ayerwords.findMany( {where: {OR: query}})
    return {results}
  }

