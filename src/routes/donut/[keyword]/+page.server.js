import prisma from '$utils/prisma'

export async function load({params}){
    const {keyword} = params
    const results = await prisma.ayerwords.findMany( {where: { text: { contains: keyword}}})
    return {results}
  }
