import prisma from '$utils/prisma'

export async function load({params}){
    // console.log(params)
    const {word} = params
    let words = word.indexOf('/') ? word.split('/') : [word]
    let query = words.length > 1 ? {OR: {words.map(w => ({text: {contains: w}}))}} : {text: {contains: w}}
    const results = await prisma.ayerwords.findMany({
        where: { OR: query}
    })
    // const rresults = await prisma.yearlines.findMany({where: { text: { contains: "April 14th"}}})
    return {results, words}
  }


